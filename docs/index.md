# SuperBLT

SuperBLT is a fork of the BLT mod-loading hook for PAYDAY 2, with a number of major
improvements, such as a [cross-platform audio API](doc/xaudio.md) and the ability to
[alter any base-game XML file](doc/tweaker.md) without the hassle of modifying bundle files
(greatly alleviating the need for Bundle Modder).

## Installation
- Download and install the [Microsoft Visual C++ 2017 Redistributable package (x86)](https://aka.ms/vs/15/release/VC_redist.x86.exe)
(you will only need to do this once, even if you later reinstall SuperBLT)
- Download the [Latest Release DLL](https://znix.xyz/random/payday-2/SuperBLT/latest-wsock.php),
and place it in your `PAYDAY 2` folder (alongside `payday2_win32_release.exe`).
- Start the game, and SuperBLT will prompt you to download the basemod. Select Yes, and it will
notify you when it's done downloading. At this point, SuperBLT has been fully installed.

However, if you've previously used vanilla BLT, you will have to:

- Delete `IPHLPAPI.dll` - This is the original BLT DLL, and you can't have both installed at
the same time due to conflicts.
- Delete `mods/base` - This is another part of the original BLT, and SuperBLT will download its
own version. However, you must first delete the old version.

## Regarding the `IPHLPAPI.dll` vs. `WSOCK32.dll` situation

Some computers, for reasons that are not apparent, refuse to load the `IPHLPAPI.dll` file as
used by vanilla BLT. This seems to primarily affect Windows 10 systems. For this reason,
SuperBLT has switched over to hooking `WSOCK32.dll` instead, which I've yet to see any trouble
from.

If, for whatever reason, you require an `IPHLPAPI.dll` version, you can download
the [IPHLPAPI Release DLL](https://znix.xyz/random/payday-2/SuperBLT/latest-release.php), and
install it like you would with the normal (i.e. `WSOCK32.dll`) version. Just don't install both
at the same time.

You can also download the [Latest Development DLL](https://znix.xyz/random/payday-2/SuperBLT/latest.php),
however you should only do this if you've been instructed to, or if you know what you're doing. Note that if you
use this DLL, you'll always be told a newer version is available by the update notifier and as such you'll have
to manually check for updates.

## Credits
- ZNixian, for writing SuperBLT
- the Restoration Crew, for testing/feedback
- JamesWilko and SirWaddles, for writing the original BLT

## Source Code
This project is Free and Open Source software, with the DLL under the GNU General
Public License version 3 (or any later version), and the basemod under the BSD license.

[DLL Source Code](https://gitlab.com/znixian/payday2-superblt)

[Basemod Source Code](https://gitlab.com/znixian/payday2-superblt-lua)

## Developer Documentation

For everything not added by SuperBLT, see [the vanilla BLT's documentation](https://payday-2-blt-docs.readthedocs.io/en/latest/).

Documentation for stuff added by SuperBLT:

- [XAudio API](doc/xaudio.md)
- [Automatic Updates](doc/updates.md)
- [XML Tweaker](doc/tweaker.md)
- [XML Configuration Files](doc/xml.md)
- [Wren API](doc/wren/intro.md)

